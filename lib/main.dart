import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_icons/flutter_icons.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Profile Card',
      home: Container(
        color: Colors.lightBlue[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 80,
              backgroundImage: AssetImage("images/profile_photo.jpg"),
            ),
            SizedBox(
              height: 20,
            ),
            Text("Okan Yıldırım",
                style:
                    GoogleFonts.audiowide(fontSize: 35, color: Colors.indigo)),
            SizedBox(
              height: 15,
            ),
            Text("Software Engineer",
                style: GoogleFonts.farsan(fontSize: 25, color: Colors.indigo)),
            Container(
              width: 200,
              child: Divider(
                height: 30,
                color: Colors.indigo,
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(horizontal: 45),
              color: Colors.indigo[400],
              child: ListTile(
                leading: Icon(
                  Icons.email,
                  color: Colors.black,
                ),
                title: Text(
                  "okan.yildirim@email.com",
                  style: TextStyle(
                      fontSize: 20,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Card(
              margin: EdgeInsets.symmetric(horizontal: 45),
              color: Colors.indigo[400],
              child: ListTile(
                leading: Icon(
                  Icons.phone,
                  color: Colors.black,
                ),
                title: Text(
                  "+90 544 544 54 54",
                  style: TextStyle(
                      fontSize: 20,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Card(
              margin: EdgeInsets.symmetric(horizontal: 45),
              color: Colors.indigo[400],
              child: ListTile(
                leading: Icon(
                  FontAwesome.gitlab,
                  color: Colors.black,
                ),
                title: Text(
                  "gitlab.com/okan.yildirim",
                  style: TextStyle(
                      fontSize: 20,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Card(
              margin: EdgeInsets.symmetric(horizontal: 45),
              color: Colors.indigo[400],
              child: ListTile(
                leading: Icon(
                  FontAwesome.linkedin,
                  color: Colors.black,
                ),
                title: Text(
                  "www.linkedin.com/in/okan-yildirim/",
                  style: TextStyle(
                      fontSize: 14,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
